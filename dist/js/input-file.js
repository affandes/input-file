(function ($) {
    $.fn.inputFile = function (options) {

        /**
         * Urutan algoritm
         * 1. Merge options + settings => settings
         * 2. Validate settings
         * 5. Init dropzone
         * 6. Init onchange
         * 7. Init ondragstart
         * 8. Init ondragover
         * 9. Init ondrop
         * 10. Get All Files
         * 11. Validate file type
         * 12. Validate file size
         * 13. Set file to input element
         * 14. Get preview of image files (multi file preview)
         * 15. Submit files
         * 14. Trigger all events
         *
         */

            // Get element
        var thisEl = $(this);
        var isValid = true;
        var msgStatus = '';

        // Extensions MIME
        var mime = {
            jpg: 'image/jpg',
            png: 'image/png',
            bmp: 'image/bmp'
        };

        console.log('Init Input File');


        console.log('1. Merge setting + options');
        // Merge settings + options
        var settings = $.extend({
            dropzone: null,
            maxSize: 2097152,
            minSize: 0,
            accept: []
        }, options);

        console.log('2. Validate settings');
        validateSettings();

        console.log('3. Init dropzone if exist');


        console.log('Multiple: ' + isMultiple());

        function validateSettings() {
            if( !validateSettingDropzone() ) {
                isValid = false;
                msgStatus = 'Invalid dropzone value';
                console.log(msgStatus);
                settings.dropzone = null;
                return false;
            } else if( !validateSettingSize(settings.minSize) ) {
                isValid = false;
                msgStatus = 'Invalid minSize value';
                console.log(msgStatus);
                settings.minSize = 0;
                return false;
            } else if( !validateSettingSize(settings.maxSize) ) {
                isValid = false;
                msgStatus = 'Invalid maxSize value';
                console.log(msgStatus);
                settings.maxSize = 2097152;
                return false;
            } else if( !validateSettingAccept() ) {
                msgStatus = 'Invalid accept value';
                return false;
            } else if( !validateSettingPreview() ) {
                msgStatus = 'Invalid preview value';
                return false;
            }
        }

        function validateSettingDropzone() {
            if( settings.dropzone !== null && typeof settings.dropzone !== 'string' ) {
                return false;
            }
            return true
        }

        function validateSettingSize(s) {
            if( typeof s !== 'number' ) {
                return false;
            }
            return true;
        }

        function validateSettingAccept() {
            var ext = settings.accept;
            if( typeof ext === 'string' ) {
                if( ext.length > 0 ) {
                    settings.accept = ext.replace(/\*|\./gi, '').replace(/( )+/gi, ' ').split(' ');
                } else {
                    settings.accept = [];
                }
            } else if( typeof ext !== 'object' ) {
                settings.accept = [];
                return false;
            }
            convertToMime();
            return true;
        }

        function convertToMime() {
            if( settings.accept.length > 0 ) {
                var ext = settings.accept;
                for(var i=0; i<ext.length; i++) {
                    ext[i] = getMime(ext[i]);
                }
                settings.accept = ext;
            }
        }

        function getMime(e) {
            var mime = mime[e];
            if( mime === undefined ) {
                return e;
            } else {
                return mime;
            }
        }

        function validateSettingPreview() {
            if( settings.preview !== null && typeof settings.preview !== 'string' ) {
                return false;
            }
            return true
        }

        // Init dropzone
        if( settings.dropzone !== null && typeof settings.dropzone === 'string') {
            $(settings.dropzone).on('drop', function (o) {
                o.preventDefault();
                o.stopPropagation();

                if( isDisabled() ) { return }

                if( getFilesFromDragDrop(o.originalEvent.dataTransfer) ) {

                    thisEl.each(function () {
                        if( isMultiple() ) {
                            this.files = o.originalEvent.dataTransfer.files;
                        } else if( o.originalEvent.dataTransfer.files.length === 1 ) {
                            this.files = o.originalEvent.dataTransfer.files;
                        } else {
                            console.log('Drop more than 1 file not supported.')
                        }
                    });

                    for(var i=0; i<o.originalEvent.dataTransfer.files.length; i++) {
                        console.log('File: ' + o.originalEvent.dataTransfer.files[i].name + ' (' + o.originalEvent.dataTransfer.files[i].size + ')');
                    }

                    setPreview();
                }

            });
            $(settings.dropzone).on('dragstart', function (o) {
                o.preventDefault();
                o.stopPropagation();
                console.log('Start');
            });
            $(settings.dropzone).on('dragover', function (o) {
                o.preventDefault();
                o.stopPropagation();
                console.log('Over');
            });
        }

        function getFilesFromDragDrop(d) {
            if(d.items) {
                for(var i=0;i<d.items.length;i++) {
                    if( d.items[i].kind === 'file' ) {
                        var f = d.items[i].getAsFile();
                        if( !validateFileType(f) ) {
                            isValid = false;
                            msgStatus = 'Invalid file type value';
                            console.log(msgStatus);
                            return false;
                        } else if( !validateFileSize(f) ) {
                            isValid = false;
                            msgStatus = 'Invalid file size value';
                            console.log(msgStatus);
                            return false;
                        }
                    } else {
                        isValid = false;
                        msgStatus = 'Invalid data type value';
                        console.log(msgStatus);
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        function getFiles(d) {
            if(d.files) {
                for(var i=0;i<d.files.length;i++) {
                    var f = d.files[i];
                    if( !validateFileType(f) ) {
                        isValid = false;
                        msgStatus = 'Invalid file type value';
                        console.log(msgStatus);
                        return false;
                    } else if( !validateFileSize(f) ) {
                        isValid = false;
                        msgStatus = 'Invalid file size value';
                        console.log(msgStatus);
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        function validateFileType(f) {
            var accept = getAccepted();
            if( accept.length === 0 ) { return true; }
            if( $.inArray(f.type, accept) >= 0 ) {
                return true;
            } else {
                isValid = false;
                msgStatus = 'File harus berekstensi ' + accept.join(', ');
                return false;
            }
        }

        function getAccepted() {
            return settings.accept;
        }

        function validateFileSize(f) {
            var min = true;
            var max = true;
            if( settings.minSize > 0 ) {
                min = f.size >= settings.minSize;
            }
            if( settings.maxSize > 0 ) {
                max = f.size <= settings.maxSize;
            }
            return min && max;
        }

        function isMultiple() {
            return thisEl.prop('multiple');
        }
        function isDisabled() {
            return thisEl.prop('disabled');
        }

        thisEl.change(function (o) {
            console.log('Add files');
            if( getFiles(this) ) {
                for(var i=0; i < this.files.length; i++) {
                    console.log('Change to ' + this.files[i].name);
                }
                $.trigger('if.onload', this.files);
                setPreview();
            }
        });

        function setPreview() {
            if( settings.preview !== null ) {
                if( thisEl[0].files.length > 0 ) {

                    var preview = $(settings.preview)[0];
                    var file    = thisEl[0].files[0];
                    var reader  = new FileReader();

                    reader.addEventListener("load", function () {
                        preview.src = reader.result;
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }


                }
            }
        }

        return this;

    };

})(jQuery);